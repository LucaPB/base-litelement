import { LitElement, html } from "lit-element";

class PersonaHeader extends LitElement{
    
    static get properties() {
        return {

        };
    }

    constructor() {
        super();
    } 

    render() {
        return html`
            <div>
                <h1>Persona Header!</h1>
            </div>
        `;
    }
}

customElements.define('persona-header', PersonaHeader);