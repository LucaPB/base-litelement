import { LitElement, html } from "lit-element";
import '../persona-header/persona-header.js';
import '../persona-main/persona-main.js';
import '../persona-footer/persona-footer.js';
import '../persona-sidebar/persona-sidebar.js';
import "../persona-stats/persona-stats.js";

class PersonaApp extends LitElement{
    
    static get properties() {
        return {
            people: {type: Array}
        };
    }

    constructor() {
        super();
    } 

    updated(changedProperties) {
        if (changedProperties.has("people")) {
            this.shadowRoot.querySelector("persona-stats").people = this.people;
        }
    }

    render() {
        return html`
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
            <persona-header></persona-header>
            <div class="row">
                <persona-sidebar @new-person="${this.newPerson}" class="col-2"></persona-sidebar>
                <persona-main @updated-people="${this.updatedPeople}" class="col-10"></persona-main>
            </div>
            <persona-footer></persona-footer>
            <persona-stats @updated-people-stats="${this.updatedPeopleStats}" ></persona-stats>
        `;
    }

    newPerson() {
        console.log("newPerson en persona-app");
        this.shadowRoot.querySelector("persona-main").showPersonForm = true;
    }

    updatedPeople(e) {
        console.log("updatedPeople en persona app");
        this.people = e.detail.people;
    }

    updatedPeopleStats(e) {
        console.log("updatedPeopleStats");
        console.log(e.detail);

        this.shadowRoot.querySelector("persona-sidebar").peopleStats = e.detail.peopleStats;
    }
}

customElements.define('persona-app', PersonaApp);