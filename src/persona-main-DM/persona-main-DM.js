import { LitElement, html } from "lit-element";

class PersonaMainDM extends LitElement{
    
    static get properties() {
        return {
            people: {type: Array}
        };
    }

    updated(changedProperties) {
        if (changedProperties.has("people")) {
            console.log("people-data-updated dispatch");
            this.dispatchEvent(
                new CustomEvent(
                    "people-data-updated", 
                    {
                        detail: {
                            people: this.people
                        }
                    }
                )
            );
        }
    }

    constructor() {
        super();

        console.log("data-manager constructor");

        this.people = [
            {
                name: "Nombre1 Apellido1",
                yearsInCompany: "10",
                photo: {
                    src: "./img/persona.jpg",
                    alt: "ALT Nombre1 Apellido1"
                },
                profile: "Profile 1"
            }, {
                name: "Nombre2 Apellido2",
                yearsInCompany: "2",
                photo: {
                    src: "./img/persona.jpg",
                    alt: "ALT Nombre2 Apellido2"
                },
                profile: "Profile 2"
            }, {
                name: "Nombre3 Apellido3",
                yearsInCompany: "5",
                photo: {
                    src: "./img/persona.jpg",
                    alt: "ALT Nombre3 Apellido3"
                },
                profile: "Profile 3"
            },  {
                name: "Nombre4 Apellido4",
                yearsInCompany: "9",
                photo: {
                    src: "./img/persona.jpg",
                    alt: "ALT Nombre4 Apellido4"
                },
                profile: "Profile 4"
            },  {
                name: "Nombre5 Apellido5",
                yearsInCompany: "1",
                photo: {
                    src: "./img/persona.jpg",
                    alt: "ALT Nombre5 Apellido5"
                },
                profile: "Profile 5"
            }
        ]
    } 

    render() {
        return html`
        `;
    }
}

customElements.define('persona-main-dm', PersonaMainDM);