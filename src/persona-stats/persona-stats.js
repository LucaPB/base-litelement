import { LitElement, html } from "lit-element";

class PersonaStats extends LitElement{
    
    static get properties() {
        return {
            people: {type: Array}
        };
    }

    updated(changedProperties) {
        console.log("updated en persona-stats");
        console.log(changedProperties);

        if (changedProperties.has("people")) {
            console.log("Ha cambiado el valor de la propiedad people en persona-stats");

            let peopleStats = this.gatherPeopleArrayInfo(this.people);
            console.log(peopleStats);

            this.dispatchEvent(
                new CustomEvent(
                    "updated-people-stats",
                    {
                        detail: {
                            peopleStats : peopleStats
                        }
                    }
                )
            )
        }
    }

    constructor() {
        super();

        this.people = [];
    } 

    gatherPeopleArrayInfo(people) {
        console.log("gatherPeopleArrayInfo");
        let peopleStats = {};
        peopleStats.numberOfPeople = people.length;

        return peopleStats;
    }
}

customElements.define('persona-stats', PersonaStats);