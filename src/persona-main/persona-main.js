import { LitElement, html } from "lit-element";
import '../persona-ficha-listado/persona-ficha-listado.js';
import "../persona-form/persona-form.js";
import "../persona-main-DM/persona-main-DM.js";

class PersonaMain extends LitElement{

    static get properties() {
        return {
            people: {type: Array},
            showPersonForm: {type: Boolean}
        };
    }

    updated(changedProperties) {
        console.log("update");
        if(changedProperties.has("showPersonForm")) {
            console.log("ha cambiado el valor de la propiedad showPersonForm en persona main");

            if (this.showPersonForm === true) {
                this.showPersonFormData();
            } else {
                this.showPersonFormList();
            }
        }
        if(changedProperties.has("people")) {
            console.log("Ha cambiado el valor de la propiedad people en persona main");

            this.dispatchEvent(
                new CustomEvent(
                    "updated-people",
                    {
                        detail: {
                            people : this.people
                        }
                    }
                )
            )
        }
    }

    constructor() {
        super();
        this.showPersonForm = false;
        this.people = []
    } 

    render() {
        return html`
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
            <persona-main-dm
                @people-data-updated="${this.getPeopleInfo}"></persona-main-dm>
            <h2 class="text-center">Persona Main!</h2>
            <div class="row" id="peopleList">
                <div class="row row-cols-1 row-cols-sm-4">
                    ${this.people.map(
                        person => html`<persona-ficha-listado 
                            fname="${person.name}"
                            yearsInCompany="${person.yearsInCompany}"
                            .photo="${person.photo}" 
                            profile="${person.profile}"
                            @delete-person="${this.deletePerson}"
                            @info-person="${this.infoPerson}"
                        ></persona-ficha-listado>` 
                    )}
                </div>    
            </div>
            <div class="row">
                    <persona-form 
                        @persona-form-close="${this.personFormClose}" 
                        @persona-form-store="${this.personFormStore}"
                        id="personForm" class="d-none border rounded border-primary"></persona-form>
            </div>
        `;
    }

    deletePerson(e) {
        console.log("delete-person");
        
        this.people = this.people.filter(
            person => person.name != e.detail.name
        )
    }

    personFormStore(e) {
        console.log("personFormStore");
        console.log("se va a almacenar una persona");
        console.log(e.detail.person);

        if(e.detail.editingPerson) {
            console.log("editando person");
            let indexOfPerson = this.people.findIndex(
                person => person.name === e.detail.person.name
            );

            this.people = this.people.map(
                person => person.name === e.detail.person.name
                ? person = e.detail.person : person);

            //if (indexOfPerson >= 0) {
            //    this.people[indexOfPerson] = e.detail.person;
            //}
        } else {
            //this.people.push(e.detail.person);

            //JS SPREAD SYNTAX
            this.people = [...this.people, e.detail.person];
            
        }

        this.showPersonForm = false; 
    }

    personFormClose() {
        console.log("personFormClose");
        console.log("se ha cerrado el formulario de la persona");
        this.showPersonForm = false;
    }

    showPersonFormData() {
        console.log("showPersonFormData")
        this.shadowRoot.getElementById("personForm").classList.remove("d-none");
        this.shadowRoot.getElementById("peopleList").classList.add("d-none");
    }
    
    showPersonFormList() {
        console.log("showPersonFormList")
        this.shadowRoot.getElementById("personForm").classList.add("d-none");
        this.shadowRoot.getElementById("peopleList").classList.remove("d-none");
    }

    infoPerson(e) {
        console.log("infoPerson "+ e.detail.name);

        let chosenPerson = this.people.filter(
            person => person.name === e.detail.name
        );

        //console.log(chosenPerson[0].name);
        
        let person = {};
        person.name = chosenPerson[0].name;
        person.profile = chosenPerson[0].profile;
        person.yearsInCompany = chosenPerson[0].yearsInCompany;

        this.shadowRoot.getElementById("personForm").person = person;
        this.shadowRoot.getElementById("personForm").editingPerson = true;
        this.showPersonForm = true; 
    }

    getPeopleInfo(e) {
        console.log("getPeopleInfo " + e);
        this.people = e.detail.people;
    }
}

customElements.define('persona-main', PersonaMain);