import { LitElement, html } from "lit-element";

class testAPI extends LitElement{
    
    static get properties() {
        return {
            movies: {type: Array} 
        };
    }

    constructor() {
        super();

        this.movies = [];

        this.getMovieData();
    } 

    render() {
        return html`
            <div>
                <div>
                    ${this.movies.map(
                        movie => html`
                        <div>La pelicula ${movie.title} , fue dirigida por ${movie.director}</div>
                        `
                    )}
                </div>
            </div>
        `;
    }

    getMovieData() {
        console.log("getMovieData");
        console.log("Obteniendo datos de las películas");

        let xhr = new XMLHttpRequest();

        xhr.onload = () => {
            if (xhr.status === 200) {
                console.log("Petición completada correctamente");
                console.log(JSON.parse(xhr.responseText));

                let APIResponse = JSON.parse(xhr.responseText);

                this.movies = APIResponse.results;
            }
        }

        xhr.open("GET", "https://swapi.dev/api/films");
        xhr.send();

        console.log("Fin de getMovieData()");
    }
}

customElements.define('test-api', testAPI);