import { LitElement, html } from "lit-element";

class PersonaFooter extends LitElement{
    
    static get properties() {
        return {

        };
    }

    constructor() {
        super();
    } 

    render() {
        return html`
            <div>
                <h5>Persona Footer!</h5>
            </div>
        `;
    }
}

customElements.define('persona-footer', PersonaFooter);